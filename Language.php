<?php

class Language {
  private $lg = array();

  public function __construct($lg = '') {
    $this->lg = parse_ini_file("de.ini");
  }

  public function get($key) {
    if(array_key_exists($key, $this->lg)) {
      return $this->lg[$key];
    }
    return "[[$key]]";
  }
}