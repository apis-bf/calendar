<?php
  include_once "LoggerClient.php";
  new LoggerClient();

  include_once "Language.php";
  include_once "Options.php";

  $languageLoader = new Language();

  function lg($key) {
    global $languageLoader;
    echo $languageLoader->get($key);
  }

  $baseurl = "https://calendar.potthast.nrw/api";
  $url = "";
  if(isset($_POST['year'])) {
    $url = $baseurl;
    $connector = "?";
    foreach ($_POST as $key => $value) {
      if($value != "") {
        $url .= $connector.$key."=".$value;
        $connector = "&";
      }
    }
  }

  $content = "";
  if($url != "") {
    $content = curl($url);
  }

  function curl($url) {
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

      $data = curl_exec($ch);
      curl_close($ch);

      return $data;
  }

$jsfilename = "calendar.txt";
  if(isset($_POST['format'])) {
    $jsfilename = "calendar.";
    switch ($_POST['format']) {
      case 'json':
        $jsfilename .= "json";
        break;
      case 'xml':
      case 'xml2':
      default:
        $jsfilename .= "xml";
        break;
    }
  };
$jsfilecontent = str_replace("\n", "\\n", $content);
?>

<!DOCTYPE html>
<html>
<head>
  <title><?php lg("CalendarAPI")?>- Alpha GUI</title>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="Keywords" content="XML,JSON,Kalender,Calendar">
  <meta name="Description" content="Userinterface for the flexible Calendar API">

  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://libs.potthast.nrw/fontawesome/5.5.0/css/all.css">
  <link rel="stylesheet" href="https://libs.potthast.nrw/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
</head>
<body>
  <div class="modal fade bd-example-modal-lg" id="impressumModal" tabindex="-1" role="dialog" aria-labelledby="impressumModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="impressumModalTitle">Impressum</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class='impressum'>
            <?php echo curl("https://webtxt.potthast.nrw/de.php?content=impressum")?>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-example-modal-lg" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="contactModalTitle">Impressum</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class='contact'>
            <p>Sie haben Fragen oder Probleme mit der Api?</p>
            <p>Sie haben Anredungen oder Wünsche?</p>
            <p>Sie haben einen Fehler entdeckt?</p>
            <p><strong>Melden Sie sich:</strong></p>
            <p><a href="mailto:info@potthast.nrw?subject=Calendar API">info@potthast.nrw</a></p>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-example-modal-lg" id="licenceModal" tabindex="-1" role="dialog" aria-labelledby="licenceModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="licenceModalTitle">Licence</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h1>MIT License</h1>
          <p>
            Copyright (c) 2018-<?php echo  date("Y" ); ?> Bernd Potthast
          </p>
          <p>
            Permission is hereby granted, free of charge, to any person obtaining a copy
            of this software and associated documentation files (the "Software"), to deal
            in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
            copies of the Software, and to permit persons to whom the Software is
            furnished to do so, subject to the following conditions:
          </p>
          <p>
            The above copyright notice and this permission notice shall be included in all
            copies or substantial portions of the Software.
          </p>
          <p>
            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
            IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
            FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
            AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
            LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
            OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
            SOFTWARE.
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<header>
    <h1><?php lg("CalendarAPI") ?></h1>
  </header>
  <main>
    <div id="settings">
      <form method="post" action="index.php">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label id="lbl_format" class="input-group-text" for="slc_format"><?php lg("format")?></label>
            </div>
            <select id="slc_format" class="custom-select" name="format">
              <?php foreach (Options::$formats as $format): ?>
                <option value="<?php echo $format; ?>"<?php echo (isset($_POST['format']) && $_POST['format'] == $format)?' selected="selected"':'' ?>><?php lg("f-".$format); ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label id="lbl_year" class="input-group-text" for="ipt_year"><?php lg("year")?></label>
          </div>
          <input id="ipt_year" class="form-control" name="year" type="number" value="<?php echo (isset($_POST['year']))?$_POST['year']:date("Y"); ?>">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label id="lbl_lang" class="input-group-text" for="slc_lang"><?php lg("language")?></label>
          </div>
          <select id="slc_lang" class="custom-select" name="language">
            <?php foreach (Options::$languages as $language): ?>
              <option value="<?php echo $language; ?>"<?php echo (isset($_POST['language']) && $_POST['language'] == $language)?' selected="selected"':'' ?>><?php lg("l-".$language)?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label id="lbl_hd" class="input-group-text" for="slc_hd"><?php lg("holidays")?></label>
          </div>
          <select id="slc_hd" class="custom-select" name="holidays">
            <?php foreach (Options::$holidays as $country => $states): ?>
              <option value="<?php echo $country; ?>"<?php echo (isset($_POST['holidays']) && $_POST['holidays'] == $country)?' selected="selected"':'' ?>><?php lg("c-".$country)?></option>
              <?php foreach ($states as $state): ?>
                <option value="<?php echo $country."-".$state; ?>"<?php echo (isset($_POST['holidays']) && $_POST['holidays'] == $country."-".$state)?' selected="selected"':'' ?>><?php lg("c-".$country."-".$state)?></option>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label id="lbl_hd" class="input-group-text" for="slc_hd"><?php lg("vacations")?></label>
          </div>
          <select id="slc_hd" class="custom-select" name="vacations">
            <?php foreach (Options::$vacations as $vacation): ?>
              <option value="<?php echo $vacation; ?>"<?php echo (isset($_POST['vacations']) && $_POST['vacations'] == $vacation)?' selected="selected"':'' ?>><?php lg("v-".$vacation)?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div>
          <label></label>
          <input type="submit" class="form-control"  value="<?php lg("load")?>">
        </div>
        <hr/>
        <div>
          <h3>Richtigkeit und Bereitstellung der Daten</h3>
          <p>
            Sämtliche Angaben sind ohne Gewähr. Ich übernehme weder Verantwortung für die Richtigkeit der Daten noch
            hafte ich für Schäden die aus der Verwendung dieser API entstehen.
            Ich behalte mir vor, die API jederzeit - ohne vorherige Ankündigung - einzustellen oder zu ändern.
          </p>
        </div>
      </form>
    </div>
    <div id="display">
      <div class="input-group">
        <input id="url" class="form-control" readonly type="text" value="<?php echo $url; ?>">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" onclick="document.getElementById('url').select();document.execCommand('copy');"<?php if(strlen($url) == 0) echo ' disabled="disabled"'?>><i class="fas fa-paste"></i></button>
          <button class="btn btn-outline-secondary" type="button" onclick="window.open('<?php echo $url; ?>', '_blank');"<?php if(strlen($url) == 0) echo ' disabled="disabled"'?>><i class="fas fa-external-link-square-alt"></i></button>
        </div>
      </div>
      <div class="input-group">
        <textarea id="code" class="form-control" readonly><?php echo $content; ?></textarea>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" onclick="document.getElementById('code').select();document.execCommand('copy');"<?php if(strlen($content) == 0) echo ' disabled="disabled"'?>><i class="fas fa-paste"></i></button>
          <button class="btn btn-outline-secondary" type="button" onclick="download()" <?php if(strlen($content) == 0) echo ' disabled="disabled"'?>><i class="fas fa-save"></i></button>
        </div>
      </div>
    </div>
  </main>
  <footer>
    <div>
      Free to use<br/>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#licenceModal">
        Lizenz
      </button>
    </div>
    <div>
      &#9400;&nbsp;Bernd&nbsp;Potthast<br/>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#impressumModal">
        Impressum
      </button>
    </div>
    <div>
      Fehler? Fragen? Anregungen?<br/>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#contactModal">
        Kontakt
      </button>
    </div>
  </footer>
  <script src="https://libs.potthast.nrw/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://libs.potthast.nrw/popper/popper.min.js"></script>
  <script src="https://libs.potthast.nrw/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script>
    function download() {
      var element = document.createElement('a');
      element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent('<?php echo $jsfilecontent; ?>'));
      element.setAttribute('download', '<?php echo $jsfilename; ?>');
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
    }
  </script>
</body>
</html>