<?php

class Options {
  public static $languages = array(
      'de',
      'en'
  );

  public static $language_default = 'en';

  public static $formats = array(
      'xml',
      'xml2',
      'json'
  );

  public static $format_default = 'xml';

  public static $holidays = array(
      '' => array(),
      'de' => array(
            'bw',
            'by',
            'be',
            'bb',
            'hb',
            'hh',
            'he',
            'mv',
            'ni',
            'nw',
            'rp',
            'sl',
            'sn',
            'st',
            'sh',
            'th'
        )
  );

  public static $vacations = array(
      '',
      'germany-nw'
  );

  public static $holiday_default = '';
  public static $vacations_default = '';

  public static $outputs = array(
      'open',
      'download',
      'url'
  );
}