## Quellen
### Schulferien NRW
https://www.schulministerium.nrw/ferienordnung-fuer-nordrhein-westfalen-fuer-die-schuljahre-bis-202930
### Uni/HS-Ferien NRW
https://www.mkw.nrw/hochschule-und-forschung/studium-und-lehre/vorlesungszeiten
### Deutsche Feiertage
https://github.com/bundesAPI/feiertage-api (https://bund.dev/apis)