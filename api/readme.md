# Calendar API

## How to use
### Example
calendar.potthast.nrw/api?year=2019&format=xml&lang=de&html=true

calendar.potthast.nrw/api?year=1995&html=true

calendar.potthast.nrw/api?year=2025&format=json&lang=en

http://localhost/p/calender/index.php?year=2019&lang=de&format=xml2&html=true&holidays=de-by

## Parameter

### year
Select the year

Integer

Required

### format
Select the output format

String

Values: 
* "xml" xml-structure with values as attributes
* "xml2" xml-structure with values as properties
* "json" json-structure

Default: "xml"

### lang
Select the language (Holidays are always in German)

String

Values: 
* "de"
* "en"

Default: "en"

### html
Enables encoding of htmlentities to display xml in the browser.

Boolean

Values: 
* "true"
* "false"

Default: "false"

### holidays
Enabled the adding of holidays (just German Holidays right now)

String

Values:
* "de" Germany
* "de-bw" Germany - Baden-Wuerttemberg
* "de-by" Germany - Bavaria
* "de-be" Germany - Berlin
* "de-bb" Germany - Brandenburg
* "de-hb" Germany - Bremen
* "de-hh" Germany - Hamburg
* "de-he" Germany - Hesse
* "de-mv" Germany - Mecklenburg Western Pomerania
* "de-ni" Germany - Lower Saxony
* "de-nw" Germany - North Rhine Westphalia
* "de-rp" Germany - Rhineland Palatinate
* "de-sl" Germany - Saarland
* "de-sn" Germany - Saxony
* "de-st" Germany - Saxony-Anhalt
* "de-sh" Germany - Schleswig-Holstein
* "de-th" Germany - Thuringia
    
Default: "de"