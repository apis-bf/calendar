<?php

class Year {
  public $no;
  public $short;
  public $months;

  public function __construct($year, $lang, $calender, $holidays, $vacations) {
    $this->no = $year;
    $this->short = date('y', strtotime($year."-01-01 00:00:00"));
    $this->months = Array();
    for ($monthno = 1; $monthno <= 12; $monthno++) {
      $this->months[$monthno] = new Month($year, $monthno, $lang, $calender, $holidays, $vacations);
    }
  }
}