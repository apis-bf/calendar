<?php

class Day {

  private $names = Array(
    'en' => Array(
      'long' => Array(
        1 => "Monday",
        2 => "Tuesday",
        3 => "Wednesday",
        4 => "Thursday",
        5 => "Friday",
        6 => "Saturday",
        7 => "Sunday",
      ),
      'short' => Array(
        1 => "Mon",
        2 => "Tue",
        3 => "Wed",
        4 => "Thu",
        5 => "Fri",
        6 => "Sat",
        7 => "Sun",
      ),
      'letter' => Array(
        1 => "M",
        2 => "T",
        3 => "W",
        4 => "T",
        5 => "F",
        6 => "S",
        7 => "S",
      ),
    ),
    'de' => Array(
        'long' => Array(
            1 => "Montag",
            2 => "Dienstag",
            3 => "Mittwoch",
            4 => "Donnerstag",
            5 => "Freitag",
            6 => "Samstag",
            7 => "Sonntag",
        ),
        'short' => Array(
            1 => "Mo",
            2 => "Di",
            3 => "Mi",
            4 => "Do",
            5 => "Fr",
            6 => "Sa",
            7 => "So",
        ),
        'letter' => Array(
            1 => "M",
            2 => "D",
            3 => "M",
            4 => "D",
            5 => "F",
            6 => "S",
            7 => "S",
        )
    ),
    'fr' => Array(
        'long' => Array(
            1 => "Lundi",
            2 => "Mardi",
            3 => "Mecredi",
            4 => "Jeudi",
            5 => "Vendredi",
            6 => "Samedi",
            7 => "Dimanche",
        ),
        'short' => Array(
            1 => "Lu",
            2 => "Ma",
            3 => "Me",
            4 => "Je",
            5 => "Ve",
            6 => "Sa",
            7 => "Di",
        ),
        'letter' => Array(
            1 => "L",
            2 => "M",
            3 => "M",
            4 => "J",
            5 => "V",
            6 => "S",
            7 => "D",
        )
    )
  );
  public $no;
  public $weekday;
  public $long;
  public $short;
  public $letter;
  public $dayinyear;
  public function __construct($year, $month, $day, $lang, $calender, $holidays, $vacations) {
    $this->no = $day;
    // WOCHENTAG
    $timestamp = strtotime("$year-$month-$day 00:00:00");
    $this->weekday = date("N", $timestamp);
    $this->long = $this->names[$lang]['long'][$this->weekday];
    $this->short = $this->names[$lang]['short'][$this->weekday];
    $this->letter = $this->names[$lang]['letter'][$this->weekday];

    // KALENDERWOCHE
    if($this->weekday == 1 || ($month == 1 && $day == 1)) {
      $this->calenderweek = date("W", $timestamp);
    }

    // TAG DES JAHRES
    $this->dayinyear = intval(date("z", $timestamp)) + 1;

    // DEUTSCHER FEIERTAG
    if(array_key_exists(date("Y-m-d", $timestamp), $holidays)) {
      $this->holiday = $holidays[date("Y-m-d", $timestamp)];
    }

    // FERIEN
    foreach ($vacations as $type => $vacationbt) {
      foreach ($vacationbt as $vacation) {
        if( strtotime($vacation->from) <= strtotime("$year-$month-$day 00:00:00") && strtotime("$year-$month-$day 00:00:00") <= strtotime($vacation->till)) {
          $fieldname = "vacation_$type";
          $this->$fieldname = true;
        }
      }
    }
  }
}