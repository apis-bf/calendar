<?php

class Month {
private $names = Array(
  'en' => Array(
    'long' => Array(
      1 => "January",
      2 => "February",
      3 => "March",
      4 => "April",
      5 => "May",
      6 => "June",
      7 => "July",
      8 => "August",
      9 => "September",
      10 => "October",
      11 => "November",
      12 => "December",
    ),
    'short' => Array(
      1 => "Jan",
      2 => "Feb",
      3 => "Mar",
      4 => "Apr",
      5 => "May",
      6 => "Jun",
      7 => "Jul",
      8 => "Aug",
      9 => "Sep",
      10 => "Oct",
      11 => "Nov",
      12 => "Dec",
    ),
    'letter' => Array(
      1 => "J",
      2 => "F",
      3 => "M",
      4 => "A",
      5 => "M",
      6 => "J",
      7 => "J",
      8 => "A",
      9 => "S",
      10 => "O",
      11 => "N",
      12 => "D",
    )
  ),
  'de' => Array(
    'long' => Array(
      1 => "Januar",
      2 => "Februar",
      3 => "März",
      4 => "April",
      5 => "Mai",
      6 => "Juni",
      7 => "Juli",
      8 => "August",
      9 => "September",
      10 => "Oktober",
      11 => "November",
      12 => "Dezember",
    ),
    'short' => Array(
      1 => "Jan",
      2 => "Feb",
      3 => "Mär",
      4 => "Apr",
      5 => "Mai",
      6 => "Jun",
      7 => "Jul",
      8 => "Aug",
      9 => "Sep",
      10 => "Okt",
      11 => "Nov",
      12 => "Dez",
    ),
    'letter' => Array(
      1 => "J",
      2 => "F",
      3 => "M",
      4 => "A",
      5 => "M",
      6 => "J",
      7 => "J",
      8 => "A",
      9 => "S",
      10 => "O",
      11 => "N",
      12 => "D",
    )
  ),
  'fr' => Array(
      'long' => Array(
          1 => "Janvier",
          2 => "Février",
          3 => "Mars",
          4 => "Avril",
          5 => "Mai",
          6 => "Juin",
          7 => "Juillet",
          8 => "Août",
          9 => "Septembre",
          10 => "Octobre",
          11 => "Novembre",
          12 => "Décembre",
      ),
      'short' => Array(
          1 => "Janv",
          2 => "Févr",
          3 => "Mars",
          4 => "Avril",
          5 => "Mai",
          6 => "Juin",
          7 => "Juil",
          8 => "Août",
          9 => "Sept",
          10 => "Oct",
          11 => "Nov",
          12 => "Déc",
      ),
      'letter' => Array(
          1 => "J",
          2 => "F",
          3 => "M",
          4 => "A",
          5 => "M",
          6 => "J",
          7 => "J",
          8 => "A",
          9 => "S",
          10 => "O",
          11 => "N",
          12 => "D",
      )
  )
);

  public $no;
  public $long;
  public $short;
  public $letter;

  public function __construct($year, $month, $lang, $calender, $holidays, $vacations) {
    $this->no = $month;
    $this->long = $this->names[$lang]['long'][$month];
    $this->short = $this->names[$lang]['short'][$month];
    $this->letter = $this->names[$lang]['letter'][$month];
    $this->days = Array();
    $days = cal_days_in_month($calender, $month, $year);
    for ($dayno = 1; $dayno <= $days; $dayno++) {
      $this->days[$dayno] = new Day($year, $month, $dayno, $lang, $calender, $holidays, $vacations);
    }
  }
}