<?php
class LoggerClient {
  private $log;

  public function __construct() {
    $this->log = new stdClass();
    $this->log->get = $_GET;
    $this->log->post = $_POST;
    $this->addServer('REQUEST_METHOD');
    $this->addServer('REQUEST_SCHEME');
    $this->addServer('REQUEST_URI');
    $this->addServer('REQUEST_TIME');
    $this->addServer('REQUEST_TIME_FLOAT');
    $this->addServer('HTTP_ACCEPT');
    $this->addServer('HTTP_ACCEPT_CHARSET');
    $this->addServer('HTTP_ACCEPT_ENCODING');
    $this->addServer('HTTP_ACCEPT_LANGUAGE');
    $this->addServer('HTTP_CONNECTION');
    $this->addServer('HTTP_HOST');
    $this->addServer('HTTP_REFERER');
    $this->addServer('HTTP_USER_AGENT');
    $this->addServer('HTTPS');
    $this->addServer('HOST');
    $this->addServer('PORT');
    $this->addServer('REMOTE_USER');
    $this->addServer('REMOTE_ADDR');
    $this->addServer('REDIRECT_REMOTE_USER');
    $this->addServer('PATH_INFO');
    $this->send();
  }

  function addServer($key) {
    if (isset($_SERVER[$key])) {
      $this->log->$key = $_SERVER[$key];
    }
  }

  function send() {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,"https://logger.potthast.nrw");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
        http_build_query(array('log' => json_encode($this->log))));

// Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
  }
}