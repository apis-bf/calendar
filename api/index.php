<?php

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
  // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
  // you want to allow, and if so:
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
    // may also be using PUT, PATCH, HEAD etc
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
    header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

  exit(0);
}
  
include_once "LoggerClient.php";
new LoggerClient();

include_once "LPLib_Feiertage_Connector.php";
include_once "entities/Year.php";
include_once "entities/Month.php";
include_once "entities/Day.php";
include_once "Parser.php";
include_once "loaders/HolidayLoader.php";
include_once "loaders/SchoolVacationLoader.php";

$languages = Array(
    'en',
    'de'
);


$yearno = intval(date('Y'));
$lang = 'en';
$calender = CAL_GREGORIAN;
$format = 'xml';
$htmlencoded = false;
$holidays = Array();
$vacations = Array();

if(isset($_GET['year']) && !is_nan(intval($_GET['year']))) {
  $yearno = intval($_GET['year']);
}
if(isset($_GET['format'])) {
  $format = $_GET['format'];
}
if(isset($_GET['language']) && in_array($_GET['language'], $languages)) {
  $lang = $_GET['language'];
}
if(isset($_GET['html']) && $_GET['html'] == 'true') {
  $htmlencoded = true;
}
if(isset($_GET['holidays']) && $_GET['holidays'] != "") {
  $holidayloader = new HolidayLoader($_GET['holidays']);
  $holidays = $holidayloader->getHolidays($yearno);
}
if(isset($_GET['vacations']) && $_GET['vacations'] != "") {
  $vacationloader = new SchoolVacationLoader($_GET['vacations']);
  $vacations = $vacationloader->loadVacations($yearno);
}

$years = Array();

$years[$yearno] = new Year($yearno, $lang, $calender, $holidays, $vacations);

$parser = new Parser();
$output = "no parser";
switch ($format) {
  case 'json':
    header('Content-Type: application/json');
    $output = $parser->toJson($years);
    break;
  case 'xml2':
    header("Content-type: text/xml");
    $output = $parser->toXmlProperties($years, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n", "");
    break;
  case 'xml':
  default:
    header("Content-type: text/xml");
    $output = $parser->toXmlAttributes($years, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n", "");
    break;
}
echo $output;