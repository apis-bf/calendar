<?php

class SchoolVacationLoader {
  /**
   * @var array country-state
   */
  private $code = Array('', '');

  public function __construct($code) {
    if (strlen($code) > 1) {
      $this->code = explode("-", $code);
    }
  }

  public function loadVacations($yearno) {
    $all = json_decode(file_get_contents("data/vacations.json"));
    try {
      $c = $this->code[0];
      $s = $this->code[1];
      $filtered = $all->$yearno->$c->$s;
    } catch (Exception $exception) {
      $filtered = array();
    }
    return $filtered;
  }
}