<?php

class HolidayLoader {
  private  $code = Array('de');
  public function __construct($code) {
    if(strlen($code) > 1) {
      $this->code = explode("-", $code);
    }
  }

  public function getHolidays($yearno) {
    switch ($this->code[0]) {
      case 'de': return $this->loadGerman($yearno); break;
      default: return Array();
        break;
    }
  }

  private function loadGerman($yearno) {
    $libfc = LPLib_Feiertage_Connector::getInstance();
    if(count($this->code) == 1) {
      return array_flip($libfc->getAlleFeiertageOhneLaender($yearno));
    } else {
      $feiertage = ($libfc->getFeiertageVonLand($yearno, $this->code[1]));
      $holidays = Array();
      foreach ($feiertage as $name => $details) {
        $holidays[$details['datum']] = $name;
      }
      return $holidays;
    }
  }
}