<?php

class Parser {
  public function toXmlAttributes($array, $output, $dept) {

    foreach ($array as $object) {
      $name = strtolower(get_class($object));
      $output .= "$dept<$name";
      foreach ($object as $key => $value) {
        if (!is_array($value)) {
          $output .= " $key=\"$value\"";
        }
      }
      $output .= ">";
      $newline = false;
      foreach ($object as $key => $value) {
        if (is_array($value)) {
          $output .= "\n";
          $newline = true;
          $output = $this->toXmlAttributes($value, $output, $dept."\t");
        }
      }
      if($newline) {
        $output .= $dept;
      }
      $output .= "</$name>\n";
    }
    return $output;
  }

  public function toXmlProperties ($array, $output, $dept) {

    foreach ($array as $object) {
      $name = strtolower(get_class($object));
      $output .= "$dept<$name>\n";
      foreach ($object as $key => $value) {
        if (!is_array($value)) {
          $output .= "$dept\t<$key>$value</$key>\n";
        }
      }
      foreach ($object as $key => $value) {
        if (is_array($value)) {
          $output = $this->toXmlProperties($value, $output, $dept."\t");
        }
      }
      $output .= $dept."</$name>\n";
    }
    return $output;
  }

  public function toJson($array) {
    return json_encode($array, JSON_PRETTY_PRINT);
  }
}